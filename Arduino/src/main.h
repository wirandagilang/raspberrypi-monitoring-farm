#ifndef main_h
#define main_h

#define ID_HARDWARE 1
#define SPARATOR '*'
#define MAX_BUFFER_SIZE 250

#define TIMER_SENSOR 10000

// Baudrate LoRa
#define SERIAL_BAUD 9600

// Humidity and temperature sensor
#define DHT_PIN A1
#define DHT_TYPE DHT22

// Moisture sensor
#define SOIL_MOISTURE_SAMPLING 11
#define MOISTURE_SENSOR_PIN A0

// Lora module
#define LORA_PORT Serial

// Perintah dari server
enum E_Task
{
    WAIT_FOR_EVENT,
    REQUEST_DATA,
    READ_SENSOR
};

// Json param
#define NODE "2"
#define TEMPERATURE "2"
#define HUMIDITY "3"
#define SOIL_MOISTURE "4"

#endif
