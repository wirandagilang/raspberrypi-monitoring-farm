#include "main.h"
#include <arduino.h>
#include <ArduinoJson.h>
#include "DHT.h"

DHT dht(DHT_PIN, DHT_TYPE);

uint8_t bufferCounter;
uint8_t systemTasks;
char bufferLORA[MAX_BUFFER_SIZE + 1];
unsigned long timerSensorLast;

uint8_t dataTemperatureSensor, dataHumiditySensor, dataMoistureSensor;

uint32_t hex2int(char *hex)
{
  uint32_t val = 0;
  while (*hex)
  {
    // get current character then increment
    uint8_t byte = *hex++;
    // transform hex character to the 4bit equivalent number, using the ascii table indexes
    if (byte >= '0' && byte <= '9')
      byte = byte - '0';
    else if (byte >= 'a' && byte <= 'f')
      byte = byte - 'a' + 10;
    else if (byte >= 'A' && byte <= 'F')
      byte = byte - 'A' + 10;
    // shift 4 to make space for new digit, and add the 4 bits of the new digit
    val = (val << 4) | (byte & 0xF);
  }
  return val;
}

boolean lora_check_request(char *buffer)
{
  // Jika Id sesuai
  if (hex2int(buffer) == ID_HARDWARE)
  {
    return true;
  }
  // Id tidak sesuai
  return false;
}

uint8_t read_temperature_sensor()
{
  float temperaturex = dht.readTemperature();
  // fail
  if (isnan(temperaturex))
  {
    return 0;
  }
  return (uint8_t)temperaturex;
}

uint8_t read_humidity_sensor()
{
  float humidityex = dht.readHumidity();
  // fail
  if (isnan(humidityex))
  {
    return 0;
  }
  return (uint8_t)humidityex;
}

uint32_t read_moisture_sensor()
{
  return analogRead(MOISTURE_SENSOR_PIN);
}

// Init system
void setup()
{
  // Init sensor
  dht.begin();
  // Init seerial
  LORA_PORT.begin(SERIAL_BAUD);
  // Init variable
  systemTasks = WAIT_FOR_EVENT;

  Serial.println("TEST");
}

// Main system
void loop()
{
  DynamicJsonDocument dataJson(1024);
  switch (systemTasks)
  {
  case WAIT_FOR_EVENT:
    // Cek ada data masuk di pin serial
    if (LORA_PORT.available() > 0)
    {
      // Ambil data masuk
      char inChar = LORA_PORT.read();
      bufferLORA[bufferCounter] = inChar;
      bufferCounter++;
      // Cek apakah server minta data ? membandingkan dari ID yang diminta
      if (inChar == SPARATOR)
      {
        bufferLORA[bufferCounter - 1] = 0;
        if (lora_check_request(bufferLORA))
        {
          systemTasks = REQUEST_DATA;
        }
        bufferCounter = 0;
      }
    }
    // Buffer counter overflow
    if (bufferCounter > MAX_BUFFER_SIZE)
    {
      bufferCounter = 0;
    }
    // timer untuk baca semua sesor per 10s
    if (millis() > (timerSensorLast + TIMER_SENSOR))
    {
      systemTasks = READ_SENSOR;
    }
    break;

  case REQUEST_DATA:
    // Send data
    dataJson[NODE] = ID_HARDWARE;
    dataJson[TEMPERATURE] = dataTemperatureSensor;
    dataJson[HUMIDITY] = dataHumiditySensor;
    dataJson[SOIL_MOISTURE] = dataMoistureSensor;
    serializeJson(dataJson, LORA_PORT);

    // Reset param
    timerSensorLast = millis();
    systemTasks = WAIT_FOR_EVENT;
    delay(2000);
    break;

  case READ_SENSOR:
    // temperature sensor
    dataTemperatureSensor = read_temperature_sensor();
    // humidity sensor
    dataHumiditySensor = read_humidity_sensor();
    // moisture sensor
    dataMoistureSensor = read_moisture_sensor();

    systemTasks = WAIT_FOR_EVENT;
    timerSensorLast = millis();
    delay(1000);
    break;
  }
}