import serial
import time
import json
import config
import thingSpeak
import ifttt

try:
    serialPort = serial.Serial('/dev/ttyUSB0', baudrate=9600, timeout=1)
except:
    print('Serial err')

class node_sensor:
    def __init__(self, idHardware, temperature, humidity, soilMoisture):

        self.idHardware = idHardware

        # Data sensor
        self.soilMoisture = soilMoisture
        self.humidity = humidity
        self.temperature = temperature

    def get_data(self):
        serialPort.write(self.idHardware)
        bufferSerial = serialPort.read_until('}')
        print(bufferSerial)

        jsonBuffer = json.loads(bufferSerial)
        time.sleep(2)
        self.temperature = int(jsonBuffer[config.TEMPERATURE])
        self.humidity = int(jsonBuffer[config.HUMIDITY])
        self.valueSoil = int(jsonBuffer[config.SOIL_MOISTURE])

# init
node1 = node_sensor('*1', '0', '0', '0')
node2 = node_sensor('*2', '0', '0', '0')

while 1:
    try:
        # Minta data ke node1
        print("minta data node 1")
        node1.get_data()

        print("minta data node 2")
        node2.get_data()

        # Kalibrasi
        node1.temperature = node1.temperature - 2
        node1.humidity = node1.humidity - 30
        node1.soilMoisture = int(node1.soilMoisture) - 100

        node2.temperature = node2.temperature - 2
        node2.humidity = node2.humidity - 30
        node2.soilMoisture = int(node2.soilMoisture) - 100

        # update data ke thinkspeak
        print("update thingSpeak")
        thingSpeak.update(node1.soilMoisture, node1.humidity, node1.temperature, node2.soilMoisture, node2.humidity, node2.temperature)

        # Cek data apakah ada yang berbahaya jika berbahaya maka kirim notifikasi
        if node1.temperature > 35 :
            ifttt.update(node1.soilMoisture, node1.humidity, node1.temperature)
        if node2.temperature > 35 :
            ifttt.update(node2.soilMoisture, node2.humidity, node2.temperature)

    except:
        print('error')

    # waiting
    time.sleep(5*60)